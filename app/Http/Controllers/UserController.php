<?php

namespace App\Http\Controllers;

use App\Helpers\JWTAuth;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Validator;
use App\User;

class UserController extends Controller
{
    public function pruebas(Request $request)
    {
        return "Acción de pruebas de userController";
    }

    public function register(Request $request)
    {

        //Obtener los datos del usuario por post
        $json = $request->input('json', null);

        $params = json_decode($json);

        $params_array = json_decode($json, true);

        //Limpiar datos

        if (!empty($params) && !empty($params_array)) {
            $params_array = array_map('trim', $params_array);

            //Validar datos usuario
            $validate = Validator::make($params_array, [
                'name'      => 'required|alpha',
                'surname'   => 'required|alpha',
                'email'     => 'required|email|unique:users',
                'password'  => 'required'
            ]);

            if ($validate->fails()) {
                $data  = array(
                    'Status' => 'error',
                    'code' => 404,
                    'message' => 'El usuario no se ha creado correctamente',
                    'errors' => $validate->errors()
                );
            } else {

                //Cifrar la contraseña
                $pwd = hash('sha256', $params->password);


                //Crear el usuario
                $user = new User();

                $user->name = $params_array['name'];
                $user->surname = $params_array['surname'];
                $user->email = $params_array['email'];
                $user->password = $pwd;
                $user->role = 'ROLE_USER';

                //Guardar usuario DB

                $user->save();
                $data  = array(
                    'Status' => 'success',
                    'code' => 200,
                    'message' => 'El usuario se ha creado correctamente',
                    'user' => $user
                );
            }
        } else {
            $data  = array(
                'Status' => 'error',
                'code' => 404,
                'message' => 'Formato incorrecto',

            );
        }


        return response()->json($data, $data['code']);
    }

    public function login(Request $request)
    {
        $jwtAuth = new JWTAuth();

        //Recibir datos por POST
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        //Validar los datos
        $validate = Validator::make($params_array, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if ($validate->fails()) {
            $signup  = array(
                'Status' => 'error',
                'code' => 404,
                'message' => 'El usuario no se ha podido loguear',
                'errors' => $validate->errors()
            );
        } else {
            //Cifrar la contraseña
            $pwd = hash('sha256', $params->password);

            //Devolver token o datos
            $signup = $jwtAuth->signup($params->email, $pwd);
            if (!empty($params->gettoken)) {
                $signup = $jwtAuth->signup($params->email, $pwd, true);
            }
        }

        return response()->json($signup, 200);
    }

    public function update(Request $request){
       $token = $request->header('Authorization');
       $jwtAuth = new JwtAuth();
       $checkToken = $jwtAuth->checkToken($token);

       if($checkToken){
           echo "<h1>Login correcto</h1>";
       }else{
        echo "<h1>Login incorrecto</h1>";
       }

       die();
    }
}
