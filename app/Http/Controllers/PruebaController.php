<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;


class PruebaController extends Controller
{
    public function testORM(){
        
        $posts = Post::all();
        foreach($posts as $post){
            echo "<h1>".$post->title."</h1>";
            echo "<span>{$post->user->name}</span>";
        }
        die();
    }
}
