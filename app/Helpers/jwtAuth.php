<?php

namespace App\Helpers;

use Iluminate\Support\Facades\DB;
use App\User;
use Firebase\JWT\JWT;

class JWTAuth
{
    public $key;

    public function __construct()
    {
        $this->key = 'esto_es_una_clave_secreta-545';
    }
    public function signup($email, $password, $getToken = null)
    {
        //Buscar si existe el usuario con sus credenciales

        $user = User::where([
            'email' => $email,
            'password' => $password
        ])->first();

        //Comprobar si son correctas(objeto)

        $singup = false;
        if (is_object($user)) {
            $singup = true;
        }

        //Generar el token con los datos del usuario identificado

        if ($singup) {
            $token = array(
                'sub'       => $user->id,
                'email'     => $user->email,
                'name'      => $user->name,
                'surname'   => $user->surname,
                'iat'       => time(),
                'exp'       => time() + (7 * 24 * 60 * 60)
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);

            //Devolver los datos decodificados o el token en función de un parámetro
            if (is_null($getToken)) {
                $data = $jwt;
            } else {
                $data = $decoded;
            }
        } else {
            $data = array(
                'status' => 'error',
                'message' => 'login incorrecto'
            );
        }

        return $data;
    }

    public function checkToken($jwt, $getIdentity = false){

        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        }catch(\UnexpectedValueException $e){
            $auth = false;
        }catch(\DomainException $e){
            $auth = false;
        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded->sub)){
            $auth = true;
        }else{
            $auth = false;
        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;
    }
}
